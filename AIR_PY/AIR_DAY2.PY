import os
import argparse
import re
from pathlib import Path

APP_VERSION = 'v0.10'

def clear_screen():
    os.system('cls')  # clear screen


def main():
 #clear_screen()
 AIR_HELLO = "hello world"
 print("1st program:",AIR_HELLO)
 # start parser
 parser = argparse.ArgumentParser(
    prog='AirArgs',
    formatter_class=argparse.RawDescriptionHelpFormatter,
    description='''%(prog)s program {},
        help to check input args'''.format(APP_VERSION),
    )
 parser.add_argument('uni', metavar='uni', type=str, help='Input edk2 uni file to produce x-Ami string')
 parser.add_argument('vfr', metavar='vfr', type=str, help='Input edk2 vfr file to produce x-Ami string')
 args = parser.parse_args()
 print("args:",args.uni)
 print("args:",args.vfr)

 funi=args.uni
 
 Uni2xAmiFile = open("AIR.PY", 'w')
 Uni2xAmiFile = open("AIR.PY".format(Path(args.vfr).resolve().stem), 'w')

 print('stem=',Path(funi).resolve().stem)
 print('name=',Path(funi).resolve().name)

 print('/=# {}', file = Uni2xAmiFile)
 print('/=# {}'.format(funi))


 Uni2xAmiFile.close()


if __name__ == "__main__":
    main()
