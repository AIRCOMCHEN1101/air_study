
# C:\Users\airco\AppData\Local\Programs\Python\Python38\Scripts --> pip3.exe
# C:\Users\C:\Users\airco\AppData\Local\Programs\Python\Python38\Scripts --> pip3.exe

# https://www.delftstack.com/zh-tw/howto/python/python-print-colored-text/
# pip3 install colorama

# pip3 install pyinstaller
# https://peaceful0907.medium.com/%E5%B0%87%E4%BD%A0%E7%9A%84python-code-%E6%89%93%E5%8C%85-pyinstaller-6777d0e06f58
# pyinstaller.exe -F XXX.py

import os
import colorama
from colorama import Fore
from colorama import Style

os.system('cls')  # clear screen

file = "RembrandtMayanLilac.veb"
findmodule = "Flash"

colorama.init()
print("Search veb file : "+Fore.RED + Style.BRIGHT + file + Style.RESET_ALL)
print("search module : "+Fore.GREEN +
      Style.BRIGHT + findmodule + Style.RESET_ALL+"\n")

modulelabel_save=""
n=1
try:
    with open(file) as f:
        context = f.readlines()
        # print(context)
        for line in context:
            # print(line)
            if (findmodule in line):
                modulelabel = line.split(';')
                if  modulelabel_save != modulelabel[2]:
                    modulelabel_save=modulelabel[2]
                    print("Find"+Fore.YELLOW + Style.BRIGHT, n , Style.RESET_ALL+"module : ", line.strip())
                    print("search module label : " + Fore.BLUE + Style.BRIGHT + modulelabel[2] + Style.RESET_ALL)
                    n+=1
except OSError as e:
    if (e.errno == 2):  # FileNotFoundError
        print("Can not find this file")
