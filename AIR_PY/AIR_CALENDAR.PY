import calendar
from typing import Counter

# print calendar
for counter in range (1,12):
    print(calendar.month(2021,counter))
print('print calendar by FOR')

counterX=1
while (counterX < 12):
    print(calendar.month(2021,counterX))
    counterX=counterX + 1
print('print calendar by WHILE')