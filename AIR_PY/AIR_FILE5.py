import os
import colorama
from colorama import Fore
from colorama import Style

DEBUG_MODE=0

os.system('cls')  # clear screen

colorama.init()
print(Fore.GREEN +Style.BRIGHT +"SA500XX search module program"+Style.RESET_ALL)

id = []             # veb name
name = []           # search componet name

path = 'AIR_FILE5.TXT'
with open(path) as f:
    for line in f:
        #print(line)
        s = line.split(':')
        id.append(s[0])
        name.append(s[1].strip())
if DEBUG_MODE == 1:
    print(id)
    print(name)

id1 = []            # search label name    
id1_temp = []       # temp save to compare
searchlabel =[]
index = 0
for veb in id:
    #print(veb)
    if "#" in id[index]:
        continue
    print(Fore.YELLOW + Style.BRIGHT +"====================================================="+Style.RESET_ALL)
    print("Check", index, "file: " , id[index])
    print("Search:       " , name[index])
    #print("====================================")
    try:
        with open(veb) as vebf:
            for vebfcontext in vebf.readlines():
                #print(vebfcontext)
                if "," in name[index]:
                    searchlabel = name[index].split(',')
                else:
                    searchlabel[0] = name[index]
                
                for x in searchlabel: 
                    #print("x=",x)
                    g = x.split('_')
                    #print("g=",g)
                    y = g[0]
                    z = g[1]
                    y = y.strip(z)
                    #break
                    if y in vebfcontext:
                        s1 = vebfcontext.split(';')
                        id1= s1[2].strip()
                        #print(id1)
                        if id1_temp != id1:
                            id1_temp = id1
                            print("found:        ", id1)
        index+=1
    except OSError as e:
        if (e.errno == 2):  # FileNotFoundError
            print(Fore.RED + Style.BRIGHT +"Can not find this VEB file"+Style.RESET_ALL)
        index+=1