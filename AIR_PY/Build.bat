@echo off

SET LOCATION="3"

SET PY_FILE=AIR_DAY1

if %LOCATION% == "0" (
    echo K-HOME
    SET WORK_FOLDER=D:\AIR_WORK\AIR_STUDY\AIR_PY
    SET PYTHON=C:\Users\AAA\AppData\Local\Programs\Python\Python38
)

if %LOCATION% == "1" (
    echo T-HOME
    SET SET WORK_FOLDER=E:\AIR_WORK\AIR_STUDY\AIR_PY
    SET PYTHON=C:\Users\airco\AppData\Local\Programs\Python\Python38
)

if %LOCATION% == "2" (
    echo T-OFFICE
    SET SET WORK_FOLDER=G:\AIR_WORK\AIR_STUDY\AIR_PY
    SET PYTHON=C:\Users\aircomchen\AppData\Local\Programs\Python\Python37
)

if %LOCATION% == "3" (
    echo AMI-NB
    SET SET WORK_FOLDER=E:\AIR_STUDY\AIR_PY
    SET PYTHON=C:\Python310
)

REM K-HOME
REM SET WORK_FOLDER=D:\AIR_WORK\AIR_STUDY\AIR_PY
REM T-HOME
REM SET WORK_FOLDER=E:\AIR_WORK\AIR_STUDY\AIR_PY
REM T-OFFICE
REM SET WORK_FOLDER=G:\AIR_WORK\AIR_STUDY\AIR_PY

REM K-HOME
REM SET PYTHON=C:\Users\AAA\AppData\Local\Programs\Python\Python38
REM T-HOME
REM SET PYTHON=C:\Users\airco\AppData\Local\Programs\Python\Python38
REM T-OFFICE
REM SET PYTHON=C:\Users\aircomchen\AppData\Local\Programs\Python\Python37
REM AMI-NB
REM SET PYTHON=C:\Python310

SET PATH=%WORK_FOLDER%;%PYTHON%;%PATH%

python.exe %PY_FILE%.PY