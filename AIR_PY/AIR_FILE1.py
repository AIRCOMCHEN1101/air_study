from os import linesep

# Python strip() 方法用於移除字符串頭尾指定的字符（默認為空格或換行符）或字符序列。 
# Python append() 方法用於在列表中添加新的對象。

surname = []
name = []

file = 'AIR_FILE1.TXT'

with open(file) as f:
    for lines in f.readlines():
        print(lines)
        s = lines.split(':')
        surname.append(s[0])
        name.append(s[1])
        name=[x.strip() for x in name]


for x in range(len(surname)):
    print ("fullname:",surname[x]+name[x])

print("surname:",surname)
print("name:",name)

f = open(surname[0]+".txt", "w")
f.write('do something')
f.close()