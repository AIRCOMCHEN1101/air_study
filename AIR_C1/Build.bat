@echo off

SET LOCATION="1"

if %LOCATION% == "0" (
    echo K-HOME
    SET WORK_FOLDER=D:\AIR_WORK\AIR_STUDY\AIR_C
)
if %LOCATION% == "1" (
    echo T-HOME
    SET WORK_FOLDER=E:\AIR_WORK\AIR_STUDY\AIR_C
)
if %LOCATION% == "2" (
    echo T-OFFICE
    SET WORK_FOLDER=G:\AIR_WORK\AIR_STUDY\AIR_C
)


SET OUTPUT_FOLDER=Build


REM SET C_FILE=AIR_BASIC
REM SET C_FILE=AIR_ABEL
SET C_FILE=AIR_ARRAY
SET C_SUB_FILE=AIR_SUB

SET PATH=%WORK_FOLDER%;%PATH%

REM delete obj and exe files
IF EXIST %OUTPUT_FOLDER%\%C_FILE%.EXE (
    echo.del %OUTPUT_FOLDER%\%C_FILE%.OBJ and %C_FILE%.EXE 
    call del %OUTPUT_FOLDER%\*.OBJ
    call del %OUTPUT_FOLDER%\%C_FILE%.EXE
) ELSE (
    echo.No %C_FILE%.OBJ/%C_SUB_FILE%.OBJ and %OUTPUT_FOLDER%\%C_FILE%.EXE
)

IF NOT EXIST %OUTPUT_FOLDER% (
    call MD %OUTPUT_FOLDER%
)

REM start to compile
call cl.exe /W4 /EHsc %C_FILE%.C
REM call cl.exe /W4 /EHsc %C_FILE%.C %C_SUB_FILE%.C /link /out:%OUTPUT_FOLDER%\%C_FILE%.EXE
REM call cl.exe /EHsc %C_FILE%.C %C_SUB_FILE%.C /link /out:%OUTPUT_FOLDER%\%C_FILE%.EXE

REM execute file
IF NOT EXIST %OUTPUT_FOLDER%\%C_FILE%.EXE (
    call cls
    call move *.obj Build
    call move *.exe Build
    call %OUTPUT_FOLDER%\%C_FILE%.EXE
) ELSE (
    call del *.OBJ
    echo.Can not find %OUTPUT_FOLDER%\%C_FILE%.EXE
)