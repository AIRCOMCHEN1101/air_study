#include <stdlib.h>
#include <stdio.h>

void sub();

char ASTR_A[] = "ABCDEFG DDD";

void main(int argc, char *argv[])
{
    int l=0,i=0,j=0,k=0;

     char strt1[]= "hello str1";
     char *strt2;
     char *strt3;
    printf("Hello AIR1 main\n");
    sub();

    if (argc <=2) {
        printf ("You don't input paramaters\n\n");
    } else {
        printf("argc = %d,\n",argc);
        for (i=0; i< argc; i++)        
            printf("argv[%d] = %s,\n\n",i,argv[i]);
    }

    strt2=strt1;
    printf ("\nstrt1=%s",strt1);
    strt2[9]='2';
    printf ("\nstrt2=%s",strt2);

    i=sizeof(strt1);
    printf("\nstrt1.sizeof.i=%d",i);

    j=strlen(strt1);
    printf("\nstrt1.strlen.j=%d",j);

    //strt3 = (char *)malloc(sizeof(char) * i);
    strt3 = malloc(sizeof(strt1)+sizeof(ASTR_A));
    //printf ("\nstrt3=%s",strt3);
    j=strlen(strt3);
    printf("\nstrt3.strlen.j=%d",j);
    j=sizeof(strt3);
    printf("\nstrt3.sizeof.j=%d",j);
// way1
//    strcpy(strt3,strt1);
//    strt2= strt3+10;
//    strcpy(strt2,ASTR_A);
// way2
// copy strt1 to strt3 and skip /n
    i=sizeof(strt1)-1;
    for (l=0; l<i; l++)
        strt3[l]=strt1[l];
// copy ASTR_A to after strt1 of strt3
    k=sizeof(ASTR_A);
    for ( i=0; i<k; i++)
        strt3[l++]=ASTR_A[i];

    printf ("\nstrt3=%s",strt3);

    free(strt3);
     
}
    